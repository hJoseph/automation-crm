package com.automation.crm.udemy.automation.application;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Ivan Alban
 */
@Configuration
@ComponentScan("com.automation.crm.udemy.automation")
public class Config {
}
