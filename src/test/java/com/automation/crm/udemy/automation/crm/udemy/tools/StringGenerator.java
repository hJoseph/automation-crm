package com.automation.crm.udemy.automation.crm.udemy.tools;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 @author Henry Calani
 */
@Component
@Scope("prototype")
//@PrototypeScope
public class StringGenerator {

    public String next() {

        return RandomStringUtils.randomAlphabetic(10);
    }

}
