package com.automation.crm.udemy.automation.crm.udemy.input;

import com.automation.crm.udemy.automation.crm.udemy.commons.Gender;
import lombok.Data;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Data
public class ClientInput {

    private Long id;

    private Boolean deleted;

    private String email;

    private String firstName;

    private  Gender gender;

    private String lastName;

    private Date lastPurchase;

    private Date createdDate;

}
