package com.automation.crm.udemy.automation.crm.udemy.testingApp;


import com.automation.crm.udemy.automation.crm.udemy.AbstractTest;
import com.automation.crm.udemy.automation.crm.udemy.annotations.ExecutionLevel;
import com.automation.crm.udemy.automation.crm.udemy.annotations.ExecutionPriority;
import lombok.Getter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Henry Joseph Calani A.
 */

@ExecutionPriority(ExecutionLevel.SECOND_MASTER)
public class UpdateClientTest extends AbstractTest {

    @Autowired
    private CreateClientCmd command;

    @Autowired
    private  EditClientCmd editClientCmd;

    @Getter
    private Integer clientId;


    @Before
    public void createClient() {
        command.execute();
        String employee = command.getResult();
        clientId =command.getPersonId();
        Assert.assertNotNull(employee);
    }

    @Test
    public void editClient() {
        editClientCmd.setClientId(clientId);
        editClientCmd.execute();
        String resultUpdate=editClientCmd.getResult();
        Assert.assertNotNull(resultUpdate);
    }
}
