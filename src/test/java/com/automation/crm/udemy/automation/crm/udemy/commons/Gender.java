package com.automation.crm.udemy.automation.crm.udemy.commons;

/**
 * @Autor Henry Joseph Calani A.
 **/
public enum Gender {
    MALE,
    FEMALE
}
