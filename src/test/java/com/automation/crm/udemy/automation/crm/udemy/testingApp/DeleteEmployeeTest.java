package com.automation.crm.udemy.automation.crm.udemy.testingApp;

import com.automation.crm.udemy.automation.crm.udemy.AbstractTest;
import com.automation.crm.udemy.automation.crm.udemy.annotations.ExecutionLevel;
import com.automation.crm.udemy.automation.crm.udemy.annotations.ExecutionPriority;
import com.automation.crm.udemy.automation.crm.udemy.commons.commonTypes;
import com.jayway.restassured.response.Response;
import io.restassured.http.ContentType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static com.jayway.restassured.RestAssured.given;

/**
 * @author Henry Joseph Calani A.
 */

@ExecutionPriority(ExecutionLevel.THIRD_MASTER)
public class DeleteEmployeeTest extends AbstractTest {


    @Autowired
    private CreateEmployeeCmd command;

    private  Integer personId;

    @Before
    public void createEmployee() {
        command.execute();
        String Employee = command.getResult();
        personId = command.getPersonId();
        Assert.assertNotNull(Employee);
    }

    @Test
    public void deleteEmployee() {

        Response response =  given ()
                .contentType(String.valueOf(ContentType.JSON))
                .when()
                .then()
                .log().ifError()
                .statusCode(200)
                .delete(commonTypes.URL_PROYECT.getKey()+"/client/"+personId);
        Assert.assertFalse(response.asString().contains("isError"));

    }

}
