package com.automation.crm.udemy.automation.crm.udemy;


import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ivan Alban
 */
public abstract class AbstractSecureTest extends AbstractTest {

    @Autowired
    private RequestSpecification loginRequest;

    protected String accountId;

    protected String token;

    @Before
    public void before() {
//        doLogin();
        setUp();
    }

    public void setUp() {
    }

    private void doLogin() {
        /*
        loginRequest.contentType(ContentType.JSON);
        Map<String, Object> loginCredentials = new HashMap<>();
        loginCredentials.put("username", "madrid@yopmail.com");
        loginCredentials.put("password", "Password1!");

        loginRequest.body(loginCredentials);

        Response response = loginRequest.post("/api/security/auth");

        accountId = response.headers().getValue(App.ResponseHeader.ACCOUNT_ID);
        token = response.headers().getValue(App.ResponseHeader.JWT_TOKEN);
        */
    }
}
