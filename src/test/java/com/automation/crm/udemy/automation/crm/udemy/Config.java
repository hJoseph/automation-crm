package com.automation.crm.udemy.automation.crm.udemy;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author Ivan Alban
 */
@Configuration
@ComponentScan("com.automation.crm.udemy.automation.crm.udemy")
@Import({
        com.automation.crm.udemy.automation.application.Config.class
})
public class Config {
}
