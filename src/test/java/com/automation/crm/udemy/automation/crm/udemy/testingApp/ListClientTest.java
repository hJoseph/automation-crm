package com.automation.crm.udemy.automation.crm.udemy.testingApp;

import com.automation.crm.udemy.automation.crm.udemy.AbstractTest;
import com.automation.crm.udemy.automation.crm.udemy.annotations.ExecutionLevel;
import com.automation.crm.udemy.automation.crm.udemy.annotations.ExecutionPriority;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Henry Joseph Calani A.
 */

@ExecutionPriority(ExecutionLevel.THIRD_MASTER)
public class ListClientTest extends AbstractTest {

    @Autowired
    private ListClientCmd listClientCmd;

    @Test
    public void listClient() {
        listClientCmd.execute();
        String resultUpdate=listClientCmd.getResult();
        Assert.assertNotNull(resultUpdate);
    }
}
