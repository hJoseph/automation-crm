package com.automation.crm.udemy.automation.crm.udemy.annotations;

/**
 * @author Henry J. Calani A.
 */
public enum ExecutionLevel {

    SYSTEM,
    HIGH,
    MIDDLE,
    LOW,
    REQUIRED,
    FIRST_MASTER,
    SECOND_MASTER,
    THIRD_MASTER,
    FIRST_DETAIL,
    SECOND_DETAIL,
    THIRD_DETAIL,
    CUSTOM
}
