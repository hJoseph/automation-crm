package com.automation.crm.udemy.automation.crm.udemy.testingApp;


import com.automation.crm.udemy.automation.crm.udemy.commons.Gender;
import com.automation.crm.udemy.automation.crm.udemy.commons.commonTypes;
import com.automation.crm.udemy.automation.crm.udemy.input.ClientInput;
import com.automation.crm.udemy.automation.crm.udemy.tools.NumberGenerator;
import com.automation.crm.udemy.automation.crm.udemy.tools.StringGenerator;

import com.jayway.restassured.response.Response;

import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

import static com.jayway.restassured.RestAssured.given;


@FixMethodOrder(MethodSorters.NAME_ASCENDING) //For Ascending order test execution
public class UpdateEmployee {

   // @Secure
    @Autowired
    private RequestSpecification request;

    @Autowired
    private NumberGenerator numberGenerator = new NumberGenerator();

    @Autowired
    private StringGenerator stringGenerator = new StringGenerator();

    @Autowired
    private ClientInput clientInput = new ClientInput();

    private  Map<String, Object> input = new HashMap<String, Object>();



    // private String employeeId2;
   // private Integer employeeId;

   /* @Before
    public void setUp() {
        input.put("gender", "FEMALE");
    }
    */

    /*
    @Before
    public void setUp() {

        input.put("deleted", true);
        input.put("email", stringGenerator.next()+"@gmail.com");
        input.put("firstName",stringGenerator.next());
        input.put("lastName", stringGenerator.next());
        input.put("position", numberGenerator.nextInteger());

        Response response =  given ()
                .contentType("application/json")
                .body(input)
                .when()
                .then()
                .log().ifError()
                .statusCode(200)
                .post(commonTypes.URL_PROYECT.getKey()+"/Employee");

       // employeeId = response.jsonPath().getLong("personid"));
        employeeId2 = response.asString();
    }
*/

    @Test
    public void updateEmployee() {

        //clientInput.setGender(Gender.FEMALE);
        Integer employeeId = 22;


        Response response =  given ()
                .contentType(String.valueOf(ContentType.JSON))
                .body(clientInput)
                .when()
                .then()
                .log().ifError()
                .statusCode(200)
                .put(commonTypes.URL_PROYECT.getKey()+"/client/"+employeeId+"?gender="+Gender.FEMALE);
        Assert.assertFalse(response.asString().contains("isError"));
    }
}
