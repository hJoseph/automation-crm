package com.automation.crm.udemy.automation.crm.udemy.testingApp;


import com.automation.crm.udemy.automation.crm.udemy.commons.Gender;
import com.automation.crm.udemy.automation.crm.udemy.commons.commonTypes;
import com.automation.crm.udemy.automation.crm.udemy.model.Client;
import com.automation.crm.udemy.automation.crm.udemy.input.ClientInput;
import com.automation.crm.udemy.automation.crm.udemy.tools.NumberGenerator;
import com.automation.crm.udemy.automation.crm.udemy.tools.StringGenerator;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.jayway.restassured.response.Response;
import lombok.Getter;
import lombok.Setter;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import static com.jayway.restassured.RestAssured.given;


@SynchronousExecution
public class EditClientCmd implements BusinessLogicCommand {

    @Autowired
    private NumberGenerator numberGenerator;
    @Autowired
    private StringGenerator stringGenerator;

    private ClientInput input;

    @Getter
    private Client instanClient;

    @Getter
    private String result;

    @Getter
    private  String personId;

    @Setter
    private Integer clientId;

    @Override
    public void execute() {

        Response response =  given ()
                .contentType("application/json")
                .when()
                .then()
                .log().ifError()
                .statusCode(200)
                .put(commonTypes.URL_PROYECT.getKey()+"/client/"+clientId+"?gender="+Gender.MALE);
        Assert.assertFalse(response.asString().contains("isError"));
        result = response.asString();

    }

}
