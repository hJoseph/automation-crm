package com.automation.crm.udemy.automation.crm.udemy.testingApp;


import com.automation.crm.udemy.automation.crm.udemy.commons.commonTypes;
import com.automation.crm.udemy.automation.crm.udemy.model.Person;
import com.automation.crm.udemy.automation.crm.udemy.input.PersonInput;
import com.automation.crm.udemy.automation.crm.udemy.tools.StringGenerator;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.jayway.restassured.response.Response;
import lombok.Getter;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import static com.jayway.restassured.RestAssured.given;



@SynchronousExecution
public class CreateEmployeeCmd implements BusinessLogicCommand {

    @Autowired
    private StringGenerator stringGenerator;

    private PersonInput input;

    @Getter
    private Person instancePerson;

    @Getter
    private String result;

    @Getter
    private  Integer personId;

    @Override
    public void execute() {

        input = new PersonInput();
        input.setFirstName(stringGenerator.next());
        input.setLastName(stringGenerator.next());
        input.setPosition(stringGenerator.next());
        input.setDeleted(Boolean.TRUE);
        input.setEmail(stringGenerator.next()+"@gmail.com");



        Response response =  given ()
                .contentType("application/json")
                .body(input)
                .when()
                .then()
                .log().ifError()
                .statusCode(200)
                .post(commonTypes.URL_PROYECT.getKey()+"/Employee");
        Assert.assertFalse(response.asString().contains("isError"));
        result = response.asString();
        instancePerson = response.as(Person.class);
        personId =  instancePerson.getId().intValue();

    }

}
