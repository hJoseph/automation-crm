package com.automation.crm.udemy.automation.crm.udemy.testingApp;


import com.automation.crm.udemy.automation.crm.udemy.commons.Gender;
import com.automation.crm.udemy.automation.crm.udemy.commons.commonTypes;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.jayway.restassured.response.Response;
import lombok.Getter;
import org.junit.Assert;

import static com.jayway.restassured.RestAssured.given;


@SynchronousExecution
public class ListClientCmd implements BusinessLogicCommand {

    @Getter
    private String result;

    @Override
    public void execute() {

        Response response =  given ()
                .contentType("application/json")
                .when()
                .then()
                .log().ifError()
                .statusCode(200)
                .get(commonTypes.URL_PROYECT.getKey()+"/client/list?gender="+Gender.MALE);
        Assert.assertFalse(response.asString().contains("isError"));
        result = response.asString();
    }

}
