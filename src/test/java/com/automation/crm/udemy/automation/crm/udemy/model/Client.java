package com.automation.crm.udemy.automation.crm.udemy.model;

import com.automation.crm.udemy.automation.crm.udemy.commons.Gender;
import lombok.Data;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Data
public class Client {
    private Long id;

    private String email;

    private String firstName;

    private String lastName;

    private Boolean deleted;

    private Gender gender;

    private Date lastPurchase;

    private Date createdDate;
}
