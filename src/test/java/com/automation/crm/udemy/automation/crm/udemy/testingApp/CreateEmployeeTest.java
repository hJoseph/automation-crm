package com.automation.crm.udemy.automation.crm.udemy.testingApp;

import com.automation.crm.udemy.automation.crm.udemy.AbstractTest;
import com.automation.crm.udemy.automation.crm.udemy.annotations.ExecutionLevel;
import com.automation.crm.udemy.automation.crm.udemy.annotations.ExecutionPriority;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Henry Joseph Calani A.
 */

@ExecutionPriority(ExecutionLevel.FIRST_MASTER)
public class CreateEmployeeTest extends AbstractTest {


    @Autowired
    private CreateEmployeeCmd command;

    @Test
    public void createEmployee() {
        command.execute();
        String Employee = command.getResult();
        Assert.assertNotNull(Employee);
    }

}
