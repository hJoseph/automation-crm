package com.automation.crm.udemy.automation.crm.udemy.testingApp;

import com.automation.crm.udemy.automation.crm.udemy.commons.Gender;
import com.automation.crm.udemy.automation.crm.udemy.commons.commonTypes;
import com.automation.crm.udemy.automation.crm.udemy.model.Client;
import com.automation.crm.udemy.automation.crm.udemy.input.ClientInput;
import com.automation.crm.udemy.automation.crm.udemy.tools.StringGenerator;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.jayway.restassured.response.Response;
import lombok.Getter;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.util.Date;

import static com.jayway.restassured.RestAssured.given;


@SynchronousExecution
public class CreateClientCmd implements BusinessLogicCommand {

    @Autowired
    private StringGenerator stringGenerator;

    private ClientInput input;

    @Getter
    private Client instanClient;

    @Getter
    private String result;

    @Getter
    private  Integer personId;

    @Override
    public void execute() {
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());

        input = new ClientInput();
        input.setFirstName(stringGenerator.next());
        input.setLastName(stringGenerator.next());
        input.setLastPurchase(timestamp);
        input.setCreatedDate(timestamp);
        input.setDeleted(Boolean.TRUE);
        input.setGender(Gender.FEMALE);
        input.setEmail(stringGenerator.next()+"@gmail.com");



        Response response =  given ()
                .contentType("application/json")
                .body(input)
                .when()
                .then()
                .log().ifError()
                .statusCode(200)
                .post(commonTypes.URL_PROYECT.getKey()+"/client");
        Assert.assertFalse(response.asString().contains("isError"));
        result = response.asString();
        instanClient = response.as(Client.class);
        personId = Math.toIntExact(instanClient.getId());

    }

}
